﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MyController : MonoBehaviour
{
    private Rigidbody2D rb2d = null;
    private float move = 0f;
    public float maxS = 11f;

    // Use this for initialization
    void Awake()
    {
        rb2d = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        move = Input.GetAxis("Horizontal");
        rb2d.velocity = new Vector2(move * maxS * Time.fixedDeltaTime, rb2d.velocity.y);
        if(Mathf.Abs (rb2d.velocity.y)<0.0001f && Input.GetButtonDown("Jump"))
        {
            rb2d.AddForce(Vector2.up * maxS);
        }
    }
}
