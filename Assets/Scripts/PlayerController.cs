﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    // Variables publicas para poder modifircar desde el inspector
    public float horizontalVelocity = 11f;
    public float jumpForce = 500f;
    public int lives = 3;
    //Variables privadas
    private Rigidbody2D rb2d = null;
    private float move = 0f;
    private Animator anim;
    private bool flipped = false;
    private bool muerto;

    private Vector3 iniPos;


    // Use this for initialization
    void Awake()
    {
        // Obtenemos el rigidbody y lo guardamos en la variable rb2d
        // para poder utilizarla más cómodamente
        rb2d = GetComponent<Rigidbody2D>();

        // Obtenemos el Animator Controller para poder modificar sus variables
        anim = GetComponent<Animator>();
        //inpos set
        iniPos = transform.position;
    }
        //Guardo posición inicial

    // Update is called once per frame
    void FixedUpdate()
    {
        if (muerto)
        {
            return;
        }
        //Miramos el input Horizontal
        move = Input.GetAxis("Horizontal");
        rb2d.velocity = new Vector2(move * horizontalVelocity * Time.fixedDeltaTime, rb2d.velocity.y);   

        if(Input.GetButtonDown("Jump") && (Mathf.Abs(rb2d.velocity.y) < 0.001f))
        {
            rb2d.AddForce(Vector2.up * jumpForce);
        }
        //Miramos si nos estamos moviendo.
        // OJO!! Nunca comparar con 0 floats, nunca será 0 perfecto, siempre hay un error de redondeo
        if(rb2d.velocity.x > 0.001f || rb2d.velocity.x < -0.001f)
        {
            if((rb2d.velocity.x < -0.001f && !flipped) || (rb2d.velocity.x > -0.001f && flipped))
            {
                flipped = !flipped;
                this.transform.rotation = Quaternion.Euler(0, flipped ? 180 : 0, 0);
            }
        }

        anim.SetFloat("hvelocity", Mathf.Abs(rb2d.velocity.x));
        anim.SetFloat("vvelocity", rb2d.velocity.y);
    }
    void OnCollisionEnter2D(Collision2D coll)
    {
        if(coll.gameObject.tag == "muerte")
        {
            lives --;
            muerto = true;
            anim.SetTrigger("dead");
        }
    }
    public void ResetMuerte()
    {
        Debug.LogError("Reseteo Muerte");
        lives = 3;
        anim.Play("ninja_chica_idle");
        muerto = false;
        transform.position = iniPos;

    }
}